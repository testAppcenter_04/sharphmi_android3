﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    internal class DefrostZoneAdapter : BaseAdapter<DefrostZone>
    {
        private Activity context;
        private List<DefrostZone> defrostZoneList;

        public DefrostZoneAdapter(Activity activity, List<DefrostZone> defrostZoneList)
        {
            this.context = activity;
            this.defrostZoneList = defrostZoneList;
        }

        public override DefrostZone this[int position] => defrostZoneList[position];

        public override int Count => defrostZoneList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = defrostZoneList[position].ToString();

			return view;
        }
    }
}