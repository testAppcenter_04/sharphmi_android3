﻿using System;
using Android.Views;
using Android.Widget;
using static Android.Views.View;

namespace SharpHmiAndroid
{
    public class Utility
    {
        public Utility()
        {
            
        }

		public static void setListViewHeightBasedOnChildren(ListView listView)
		{
			IListAdapter listAdapter = listView.Adapter;
			if (listAdapter == null)
			{
				// pre-condition
				return;
			}

			int totalHeight = 0;
			int desiredWidth = MeasureSpec.MakeMeasureSpec(listView.Width, MeasureSpecMode.AtMost);
			for (int i = 0; i < listAdapter.Count; i++)
			{
				View listItem = listAdapter.GetView(i, null, listView);
				listItem.Measure(desiredWidth, MeasuredHeightStateShift);
				totalHeight += listItem.MeasuredHeight;
			}

			ViewGroup.LayoutParams paramsa = listView.LayoutParameters;
			paramsa.Height = totalHeight + (listView.DividerHeight * (listAdapter.Count - 1));
			listView.LayoutParameters = paramsa;
			listView.RequestLayout();
		}
    }
}
