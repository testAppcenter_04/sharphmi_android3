﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
	public class SoftButtonCapabilitiesAdapter : BaseAdapter<SoftButtonCapabilities>
	{
        List<SoftButtonCapabilities> softButtonCapabilities;
		Activity context;
        private String ButtonText = "SoftButtonCap ";

		public SoftButtonCapabilitiesAdapter(Activity act, List<SoftButtonCapabilities> list) : base()
		{
			softButtonCapabilities = list;
			context = act;
		}

		public override SoftButtonCapabilities this[int position] => softButtonCapabilities[position];

		public override int Count => softButtonCapabilities.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = ButtonText + (position + 1);

			return view;
		}
	}
}
