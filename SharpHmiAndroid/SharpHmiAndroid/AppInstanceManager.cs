using System;
using System.Linq;
using System.Runtime.CompilerServices;
using HmiApiLib.Interfaces;
using HmiApiLib.Proxy;
using HmiApiLib;
using System.Collections.Generic;
using HmiApiLib.Base;
using Android.Graphics;
using Java.IO;
using System.IO;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;
using HmiApiLib.Controllers.BasicCommunication.IncomingRequests;
using HmiApiLib.Controllers.Navigation.IncomingNotifications;
using HmiApiLib.Controllers.SDL.IncomingNotifications;
using HmiApiLib.Controllers.RC.IncomingRequests;
using HmiApiLib.Controllers.Buttons.IncomingRequests;
using HmiApiLib.Controllers.SDL.IncomingResponses;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
	public class AppInstanceManager : ProxyHelper, IConnectionListener, IDispatchingHelper<LogMessage>
	{
		private static volatile AppInstanceManager instance;
		private static object syncRoot = new Object();
		public MessageAdapter _msgAdapter = null;
		public static Boolean bRecycled = false;

		public static Boolean appResumed = false;

		private AppSetting appSetting = null;
		public static List<AppItem> appList = new List<AppItem>();
		AppUiCallback appUiCallback;
		public static Dictionary<int, List<RpcRequest>> menuOptionListUi = new Dictionary<int, List<RpcRequest>>();
		public static Dictionary<int, List<string>> appIdPutfileList = new Dictionary<int, List<string>>();
		public static Dictionary<int, string> appIdPolicyIdDictionary = new Dictionary<int, string>();
        public static Dictionary<int, List<int?>> commandIdList = new Dictionary<int, List<int?>>();
        public enum ConnectionState {CONNECTED, DISCONNECTED, NOT_INITIALIZED }
        public static ConnectionState currentState;

		public enum SelectionMode { NONE, DEBUG_MODE, BASIC_MODE };
		private SelectionMode UserSelectedMode = SelectionMode.NONE;

		public static AppInstanceManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
                        if (instance == null) {
							currentState = ConnectionState.NOT_INITIALIZED;
							instance = new AppInstanceManager();
						}
					}
				}
				else
				{
					bRecycled = true;
				}

				return instance;
			}
		}

		private AppInstanceManager()
		{

		}

		internal void setAppUiCallback(AppUiCallback callback)
		{
			appUiCallback = callback;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public void setAppSetting(AppSetting appSetting)
		{
			this.appSetting = appSetting;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public AppSetting getAppSetting()
		{
			return this.appSetting;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public void setMsgAdapter(MessageAdapter messageAdapter)
		{
			this._msgAdapter = messageAdapter;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public MessageAdapter getMsgAdapter()
		{
			return this._msgAdapter;
		}

		public void setupConnection(String ipAddr, int portNum, InitialConnectionCommandConfig initialConnectionCommandConfig)
		{
            UserSelectedMode = appSetting.getSelectedMode();
            if (UserSelectedMode == SelectionMode.BASIC_MODE)
            {
                initConnectionManager(ipAddr, portNum, this, this, null, initialConnectionCommandConfig);
            }
            else if (UserSelectedMode == SelectionMode.DEBUG_MODE)
            {
                initConnectionManager(ipAddr, portNum, this, this, this, initialConnectionCommandConfig);
            }
		}

		public void onOpen()
		{
            // Handle logic for Callback triggered when Socket is Opened.
            //Console.WriteLine("Debug: onOpen()");
            currentState = ConnectionState.CONNECTED;
		}

		public void onClose()
		{
			// Handle logic for Callback triggered when Socket is Opened.
			//Console.WriteLine("Debug: onClose()");
            currentState = ConnectionState.DISCONNECTED;
		}

		public void onError()
		{
			// Handle logic for Callback triggered when Socket is Opened.
			//Console.WriteLine("Debug: onError()");
			currentState = ConnectionState.DISCONNECTED;
		}

		private void addMessageToUI(LogMessage message)
		{
			if (_msgAdapter == null) return;

			_msgAdapter.addMessage(message);
		}

		public void dispatch(LogMessage message)
		{
			addMessageToUI(message);
		}

		public void handleDispatchingError(string info, Exception ex)
		{
			LogMessage logMessage = new StringLogMessage(info);
			addMessageToUI(logMessage);
		}

		public void handleQueueingError(string info, Exception ex)
		{
			LogMessage logMessage = new StringLogMessage(info);
			addMessageToUI(logMessage);
		}

		//UI interface callbacks
		public override void onUiSetAppIconRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetAppIcon msg)
		{
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon>(((MainActivity)appUiCallback), tmpObj.getMethod());
            AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();

            if (null == tmpObj)
			{
				if (null != appUiCallback)
				{
					int appId = -1;
					if (appIdPutfileList.ContainsKey((int)msg.getAppId()))
					{
						appId = (int)msg.getAppId();
					}
					else
					{
						appId = getCorrectAppId(msg.getAppId());
					}
					if (appId != -1 && selectionMode == SelectionMode.BASIC_MODE)
					{
						for (int i = 0; i < appIdPutfileList[appId].Count; i++)
						{
							if (appIdPutfileList[appId].Contains(msg.getAppIcon().getValue()))
							{
								for (int j = 0; j < appList.Count; j++)
								{
									if ((appList[j].getAppID() == appId) || (appList[j].getAppID() == msg.getAppId()))
									{
										Bitmap image = null;
										try
										{
											image = BitmapFactory.DecodeStream(getPutfile(msg.getAppIcon().getValue()));
											appList[j].setAppIcon(image);
											appUiCallback.setDownloadedAppIcon();
											sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
										}
										catch (Exception ex)
										{
											sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_DATA));
										}
										return;
									}
								}
								break;
							}
						}
					}
					else
					{
                        //default to success
                        sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
						return;
					}
					sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.DATA_NOT_AVAILABLE));
				}
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public int getCorrectAppId(int? matchValue)
		{
			int appId = -1;

			if (matchValue == null) return appId;

			if (appIdPolicyIdDictionary.ContainsKey(matchValue.Value))
			{
				appId = int.Parse(appIdPolicyIdDictionary[matchValue.Value]);
			}

			if (appIdPolicyIdDictionary.ContainsValue(matchValue.Value.ToString()))
			{
				appId = appIdPolicyIdDictionary.FirstOrDefault(x => x.Value == matchValue.Value.ToString()).Key;
			}

			return appId;
		}

		public override void onUiShowRequest(HmiApiLib.Controllers.UI.IncomingRequests.Show msg)
		{
			int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.Show tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Show();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Show>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
				sendRpc(BuildRpc.buildUiShowResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
				appUiCallback.onUiShowRequestCallback(msg);
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
		}

		public override void onUiAddCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand msg)
		{
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
                int appID = (int)msg.getAppId();
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data;
				    List<int?> cmdIdList;
				    if (menuOptionListUi.ContainsKey(appID))
				    {
					    data = menuOptionListUi[appID];
					    data.Add(msg);
					    menuOptionListUi.Remove(appID);
				    }
				    else
				    {
					    data = new List<RpcRequest>();
					    data.Add(msg);
				    }
				    menuOptionListUi.Add(appID, data);

				    if (commandIdList.ContainsKey(appID))
				    {
					    cmdIdList = commandIdList[appID];
					    cmdIdList.Add(msg.getCmdId());
					    commandIdList.Remove(appID);
				    }
				    else
				    {
					    cmdIdList = new List<int?>();
					    cmdIdList.Add(msg.getCmdId());
				    }
				    commandIdList.Add(appID, cmdIdList);

				    appUiCallback.refreshOptionsMenu();
                }
                sendRpc(BuildRpc.buildUiAddCommandResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
            }
            else
            {
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
            }
		}

		public override void onUiAlertRequest(HmiApiLib.Controllers.UI.IncomingRequests.Alert msg)
		{
			int corrId = msg.getId();
			int? appId = msg.getAppId();

			sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.ALERT, appId));

            HmiApiLib.Controllers.UI.OutgoingResponses.Alert tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Alert();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Alert>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
                appUiCallback.onUiAlertRequestCallback(msg);
                sendRpc(BuildRpc.buildUiAlertResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
            sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, appId));
		}

		public override void onUiPerformInteractionRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformInteraction msg)
		{
            if (null != msg.getChoiceSet())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					appUiCallback.onUiPerformInteractionRequestCallback(msg);
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
            else
            {
                
            }
		}

		public override void onUiGetLanguageRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getUIGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildUiGetLanguageResponse(corrId, Language.EN_US, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiDeleteCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand msg)
		{
			int corrId = msg.getId();
			
            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();

                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data = new List<RpcRequest>();
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        foreach (RpcRequest req in data)
                        {
                            if (req is HmiApiLib.Controllers.UI.IncomingRequests.AddCommand)
                            {
                                if (((HmiApiLib.Controllers.UI.IncomingRequests.AddCommand)req).getCmdId() == msg.getCmdId())
                                {
                                    data.Remove(req);
                                    break;
                                }
                            }
                        }
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);
                    appUiCallback.refreshOptionsMenu();
                }
				sendRpc(BuildRpc.buildUiDeleteCommandResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiIsReadyRequest(HmiApiLib.Controllers.UI.IncomingRequests.IsReady msg)
		{
            if (appSetting.getUIIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.UI, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//TTS interface callbacks
		public override void onTtsSpeakRequest(HmiApiLib.Controllers.TTS.IncomingRequests.Speak msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.Speak tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.Speak();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.Speak>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildTtsSpeakResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
				appUiCallback.onTtsSpeakRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsStopSpeakingRequest(HmiApiLib.Controllers.TTS.IncomingRequests.StopSpeaking msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildTtsStopSpeakingResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsGetLanguageRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getTTSGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildTtsGetLanguageResponse(corrId, Language.EN_US, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsIsReadyRequest(HmiApiLib.Controllers.TTS.IncomingRequests.IsReady msg)
		{
            if (appSetting.getTTSIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.TTS, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//VR interface callbacks
		public override void onVrAddCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.AddCommand msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand>(((MainActivity)appUiCallback), tmpObj.getMethod());
            AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();

            if (null == tmpObj)
			{
				int? cmdId = msg.getCmdId();
				int? grammerId = msg.getGrammarId();

				if ((vrGrammerAddCommandDictionary != null) && (grammerId != null)
					&& (cmdId != null) && (grammerId != -1) && selectionMode == SelectionMode.BASIC_MODE)
				{
					vrGrammerAddCommandDictionary.Add((int)grammerId, (int)cmdId);
				}

				sendRpc(BuildRpc.buildVrAddCommandResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrGetLanguageRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getVRGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildVrGetLanguageResponse(corrId, Language.EN_US, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrDeleteCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.DeleteCommand msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVrDeleteCommandResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrIsReadyRequest(HmiApiLib.Controllers.VR.IncomingRequests.IsReady msg)
		{
            if (appSetting.getVRIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.VR, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrPerformInteractionRequest(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction msg)
		{
            int corrId = msg.getId();
			HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				if (null != msg.getGrammarID())
				{
					int returnVal = getVrAddCommandId(msg.getGrammarID());

					if (returnVal != -1)
					{
						sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, returnVal, HmiApiLib.Common.Enums.Result.SUCCESS));
					}
					else
					{
						sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, null, HmiApiLib.Common.Enums.Result.GENERIC_ERROR));
					}
				}
				else
				{
					// need to update choice id - currently set to 1 for SUCCESS
					sendRpc(BuildRpc.buildVrPerformInteractionResponse(msg.getId(), 1, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		//Navigation interface callbacks
		public override void onNavIsReadyRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.IsReady msg)
		{
            if (appSetting.getNavigationIsReady()) 
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.Navigation, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//VehicleInfo interface callbacks
		public override void onVehicleInfoIsReadyRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.IsReady msg)
		{
            if (appSetting.getVIIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.VehicleInfo, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//Bc interface callbacks
		public override void onBcAppRegisteredNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnAppRegistered msg)
		{
			appList.Add(new AppItem(msg.getApplication().appName, msg.getApplication().appID));
			appIdPolicyIdDictionary.Add(msg.getApplication().getAppID(), msg.getApplication().getPolicyAppID());
			if (null != appUiCallback)
				appUiCallback.onBcAppRegisteredNotificationCallback(true);
		}

		public override void onBcAppUnRegisteredNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnAppUnregistered msg)
		{
			int appID = (int)msg.getAppId();
			for (int i = 0; i < appList.Count; i++)
			{
				if ((appList[i].getAppID() == appID) || (appList[i].getAppID() == getCorrectAppId(appID)))
				{
					int tmpAppId = appID;

					if (appList[i].getAppID() == getCorrectAppId(tmpAppId))
					{
						tmpAppId = getCorrectAppId(tmpAppId);
					}

					appList.RemoveAt(i);
					i--;
				}
			}

			if (appIdPutfileList.ContainsKey(appID) || appIdPutfileList.ContainsKey(getCorrectAppId(appID)))
			{
				int tmpAppId = appID;
				if (appIdPutfileList.ContainsKey(getCorrectAppId(tmpAppId)))
				{
					tmpAppId = getCorrectAppId(tmpAppId);
				}

				deletePutfileDirectory(appIdPutfileList[tmpAppId][0]);
				appIdPutfileList[tmpAppId].Clear();
				appIdPutfileList.Remove(tmpAppId);
				appIdPolicyIdDictionary.Remove(tmpAppId);
			}

			if (null != appUiCallback)
				appUiCallback.onBcAppRegisteredNotificationCallback(false);
		}

		public override void onBcMixingAudioSupportedRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.MixingAudioSupported msg)
		{
            if (appSetting.getBCMixAudioSupport()) {
                int corrId = msg.getId();
				HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
				tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildBasicCommunicationMixingAudioSupportedResponse(corrId, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void OnButtonSubscriptionNotification(OnButtonSubscription msg)
		{
            appUiCallback.OnButtonSubscriptionNotificationCallback(msg);
		}

		public override void onUiAddSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu msg)
		{
			int corrId = msg.getId();
            int appID = (int)msg.getAppId();
            HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data;
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        data.Add(msg);
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    else
                    {
                        data = new List<RpcRequest>();
                        data.Add(msg);
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);
                    appUiCallback.refreshOptionsMenu();
                }         
			    sendRpc(BuildRpc.buildUiAddSubMenuResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
		}

		public override void onUiChangeRegistrationRequest(HmiApiLib.Controllers.UI.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
            HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiChangeRegistrationResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiClosePopUpRequest(HmiApiLib.Controllers.UI.IncomingRequests.ClosePopUp msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiClosePopUpResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiDeleteSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteSubMenu msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                {
                    List<RpcRequest> data = new List<RpcRequest>();
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        foreach (RpcRequest req in data)
                        {
                            if (req is HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu)
                            {
                                if (((HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu)req).getMenuID() == msg.getMenuID())
                                {
                                    data.Remove(req);
                                    break;
                                }
                            }
                        }
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);
                    appUiCallback.refreshOptionsMenu();
                }
				sendRpc(BuildRpc.buildUiDeleteSubMenuResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiEndAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.EndAudioPassThru msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiEndAudioPassThruResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiGetCapabilitiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getUIGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    DisplayCapabilities displayCapabilities = new DisplayCapabilities();
                    displayCapabilities.displayType = DisplayType.SDL_GENERIC;
                    displayCapabilities.graphicSupported = true;
                    List<String> templateList = new List<String>
                    {
                        "Template1",
                        "Template2"
                    };
                    displayCapabilities.templatesAvailable = templateList;
                    displayCapabilities.numCustomPresetsAvailable = 10;
                    TextField field1 = new TextField();
                    field1.name = TextFieldName.mainField1;
                    field1.characterSet = CharacterSet.TYPE2SET;
                    field1.width = 500;
                    field1.rows = 1;

                    TextField field2 = new TextField();
                    field2.name = TextFieldName.mainField2;
                    field2.characterSet = CharacterSet.TYPE2SET;
                    field2.width = 500;
                    field2.rows = 1;

                    TextField field3 = new TextField();
                    field3.name = TextFieldName.mainField3;
                    field3.characterSet = CharacterSet.TYPE2SET;
                    field3.width = 500;
                    field3.rows = 1;

                    TextField field4 = new TextField();
                    field4.name = TextFieldName.mainField4;
                    field4.characterSet = CharacterSet.TYPE2SET;
                    field4.width = 500;
                    field4.rows = 1;

                    List<TextField> textFieldList = new List<TextField>
                    {
                        field1,field2,field3,field4
                    };
                    displayCapabilities.textFields = textFieldList;

                    ImageResolution imageRes = new ImageResolution();
                    imageRes.resolutionWidth = 64;
                    imageRes.resolutionHeight = 64;

                    List<FileType> fileTypeList = new List<FileType>
                    {
                        FileType.AUDIO_AAC,FileType.AUDIO_MP3,FileType.AUDIO_WAVE,FileType.BINARY,FileType.GRAPHIC_BMP,FileType.GRAPHIC_JPEG,FileType.GRAPHIC_PNG,FileType.JSON
                    };

                    ImageField imageField1 = new ImageField();
                    imageField1.name = ImageFieldName.softButtonImage;
                    imageField1.imageResolution = imageRes;
                    imageField1.imageTypeSupported = fileTypeList;

                    ImageField imageField2 = new ImageField();
                    imageField2.name = ImageFieldName.choiceImage;
                    imageField2.imageResolution = imageRes;
                    imageField2.imageTypeSupported = fileTypeList;

                    ImageField imageField3 = new ImageField();
                    imageField3.name = ImageFieldName.graphic;
                    imageField3.imageResolution = imageRes;
                    imageField3.imageTypeSupported = fileTypeList;

                    ImageField imageField4 = new ImageField();
                    imageField3.name = ImageFieldName.appIcon;
                    imageField3.imageResolution = imageRes;
                    imageField3.imageTypeSupported = fileTypeList;

                    List<ImageField> imageFieldList = new List<ImageField>
                    {
                        imageField1,imageField2,imageField3,imageField4
                    };
                    displayCapabilities.imageFields = imageFieldList;

                    List<MediaClockFormat> mediaClockFormatList = new List<MediaClockFormat>
                    {
                        MediaClockFormat.CLOCK1,MediaClockFormat.CLOCK2,MediaClockFormat.CLOCK3,MediaClockFormat.CLOCKTEXT1,MediaClockFormat.CLOCKTEXT2,MediaClockFormat.CLOCKTEXT3,MediaClockFormat.CLOCKTEXT4
                    };
                    displayCapabilities.mediaClockFormats = mediaClockFormatList;

                    List<ImageType> imageTypeList = new List<ImageType>
                    {
                        ImageType.DYNAMIC,ImageType.STATIC
                    };
                    displayCapabilities.imageCapabilities = imageTypeList;

                    ImageResolution screenParamRes = new ImageResolution();
                    screenParamRes.resolutionWidth = 800;
                    screenParamRes.resolutionHeight = 480;
                    ScreenParams screenParams = new ScreenParams();
                    screenParams.resolution = screenParamRes;
                    TouchEventCapabilities touchCaps = new TouchEventCapabilities();
                    touchCaps.doublePressAvailable = true;
                    touchCaps.pressAvailable = true;
                    touchCaps.pressAvailable = true;

                    screenParams.touchEventAvailable = touchCaps;

                    displayCapabilities.screenParams = screenParams;
                    displayCapabilities.numCustomPresetsAvailable = 10;

                    AudioPassThruCapabilities audioPassThruCapabilities = new AudioPassThruCapabilities();
                    audioPassThruCapabilities.audioType = AudioType.PCM;
                    audioPassThruCapabilities.bitsPerSample = BitsPerSample.RATE_8_BIT;
                    audioPassThruCapabilities.samplingRate = SamplingRate.RATE_16KHZ;

                    HMICapabilities hMICapabilities = new HMICapabilities();
                    hMICapabilities.navigation = true;
                    hMICapabilities.phoneCall = true;

                    SoftButtonCapabilities softButtonCapabilities1 = new SoftButtonCapabilities();
                    softButtonCapabilities1.imageSupported = true;
                    softButtonCapabilities1.longPressAvailable = true;
                    softButtonCapabilities1.upDownAvailable = true;
                    softButtonCapabilities1.shortPressAvailable = true;

                    SoftButtonCapabilities softButtonCapabilities2 = new SoftButtonCapabilities();
                    softButtonCapabilities2.imageSupported = true;
                    softButtonCapabilities2.longPressAvailable = true;
                    softButtonCapabilities2.upDownAvailable = true;
                    softButtonCapabilities2.shortPressAvailable = true;

                    SoftButtonCapabilities softButtonCapabilities3 = new SoftButtonCapabilities();
                    softButtonCapabilities3.imageSupported = true;
                    softButtonCapabilities3.longPressAvailable = true;
                    softButtonCapabilities3.upDownAvailable = true;
                    softButtonCapabilities3.shortPressAvailable = true;

                    SoftButtonCapabilities softButtonCapabilities4 = new SoftButtonCapabilities();
                    softButtonCapabilities4.imageSupported = true;
                    softButtonCapabilities4.longPressAvailable = true;
                    softButtonCapabilities4.upDownAvailable = true;
                    softButtonCapabilities4.shortPressAvailable = true;

                    List<SoftButtonCapabilities> softButtonCapsList = new List<SoftButtonCapabilities>()
                    {
                        softButtonCapabilities1,softButtonCapabilities2,softButtonCapabilities3,softButtonCapabilities4
                    };              

                    sendRpc(BuildRpc.buildUiGetCapabilitiesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, displayCapabilities, audioPassThruCapabilities, HmiZoneCapabilities.FRONT, softButtonCapsList, hMICapabilities));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiGetSupportedLanguagesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getUIGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    List<Language> languages = new List<Language>
                    {
                        Language.EN_US,Language.EN_GB,Language.ES_MX,Language.FR_CA,Language.ES_ES,Language.DE_DE
                    };

                    sendRpc(BuildRpc.buildUiGetSupportedLanguagesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, languages));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiPerformAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformAudioPassThru msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiPerformAudioPassThruResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiScrollableMessageRequest(HmiApiLib.Controllers.UI.IncomingRequests.ScrollableMessage msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiScrollableMessageResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
				appUiCallback.onUiScrollableMessageRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetDisplayLayoutRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetDisplayLayout msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiSetDisplayLayoutResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null, null, null, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetGlobalPropertiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiSetGlobalPropertiesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetMediaClockTimerRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetMediaClockTimer msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiSetMediaClockTimerResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
				appUiCallback.onUiSetMediaClockTimerRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiShowCustomFormRequest(HmiApiLib.Controllers.UI.IncomingRequests.ShowCustomForm msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildUiShowCustomFormResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSliderRequest(HmiApiLib.Controllers.UI.IncomingRequests.Slider msg)
		{
            int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				appUiCallback.onUiSliderRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsChangeRegistrationRequest(HmiApiLib.Controllers.TTS.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildTTSChangeRegistrationResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsGetCapabilitiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getTTSGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsGetSupportedLanguagesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getTTSGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    List<Language> languages = new List<Language>
                    {
                        Language.EN_US,
                        Language.EN_GB,
                        Language.ES_MX,
                        Language.FR_CA,
                        Language.ES_ES,
                        Language.DE_DE
                    };

                    sendRpc(BuildRpc.buildTTSGetSupportedLanguagesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, languages));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsSetGlobalPropertiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.SetGlobalProperties msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildTTSSetGlobalPropertiesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrChangeRegistrationRequest(HmiApiLib.Controllers.VR.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVrChangeRegistrationResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrGetCapabilitiesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getVRGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    List<VrCapabilities> vrCapsList = new List<VrCapabilities>
                    {
                        VrCapabilities.TEXT
                    };
                    
                    sendRpc(BuildRpc.buildVrGetCapabilitiesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, vrCapsList));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrGetSupportedLanguagesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getVRGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    List<Language> languages = new List<Language>
                    {
                        Language.EN_US,Language.EN_GB,Language.ES_MX,Language.FR_CA,Language.ES_ES,Language.DE_DE
                    };

                    sendRpc(BuildRpc.buildVrGetSupportedLanguagesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, languages));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onNavAlertManeuverRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.AlertManeuver msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavAlertManeuverResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavGetWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.GetWayPoints msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavGetWayPointsResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavSendLocationRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SendLocation msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavSendLocationResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavShowConstantTBTRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.ShowConstantTBT msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavShowConstantTBTResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavStartAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartAudioStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavStartAudioStreamResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavStartStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavStartStreamResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavStopAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopAudioStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavStopAudioStreamResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavStopStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavStopStreamResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavSubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SubscribeWayPoints msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavSubscribeWayPointsResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavUnsubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavUnsubscribeWayPointsResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavUpdateTurnListRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UpdateTurnList msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildNavUpdateTurnListResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoDiagnosticMessageRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.DiagnosticMessage msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVehicleInfoDiagnosticMessageResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoGetDTCsRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetDTCs msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVehicleInfoGetDTCsResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoGetVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetVehicleData msg)
		{
            if (appSetting.getVIGetVehicleData())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{

                    GPSData gPSData = new GPSData();
                    gPSData.longitudeDegrees = -180;
                    gPSData.latitudeDegrees = -90;
                    gPSData.utcYear = 2017;
                    gPSData.utcMonth = 1;
                    gPSData.utcDay = 1;
                    gPSData.utcHours = 0;
                    gPSData.utcMinutes = 0;
                    gPSData.utcSeconds = 0;
                    gPSData.compassDirection = CompassDirection.EAST;
                    gPSData.pdop = 0;
                    gPSData.hdop = 0;
                    gPSData.vdop = 0;
                    gPSData.actual = true;
                    gPSData.satellites = 1;
                    gPSData.dimension = Dimension.Dimension_2D;
                    gPSData.altitude = -10000;
                    gPSData.heading = 0;
                    gPSData.speed = 100;

                    TireStatus tireStatus = new TireStatus();
                    tireStatus.pressureTelltale = WarningLightStatus.OFF;
                    SingleTireStatus singleTire = new SingleTireStatus();
                    singleTire.status = ComponentVolumeStatus.NORMAL;
                    tireStatus.leftFront = singleTire;
                    tireStatus.rightFront = singleTire;
                    tireStatus.leftRear = singleTire;
                    tireStatus.rightRear = singleTire;
                    tireStatus.innerLeftRear = singleTire;
                    tireStatus.innerRightRear = singleTire;

                    BeltStatus beltStatus = new BeltStatus();
                    beltStatus.driverBeltDeployed = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.passengerBeltDeployed = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.passengerBuckleBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.driverBuckleBelted = VehicleDataEventStatus.YES;
                    beltStatus.leftRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.passengerChildDetected = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.rightRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.middleRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.middleRow3BuckleBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.leftRearInflatableBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.rightRearInflatableBelted = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.middleRow1BeltDeployed = VehicleDataEventStatus.NO_EVENT;
                    beltStatus.middleRow1BuckleBelted = VehicleDataEventStatus.NO_EVENT;

                    BodyInformation bodyInformation = new BodyInformation();
                    bodyInformation.parkBrakeActive = false;
                    bodyInformation.ignitionStableStatus = IgnitionStableStatus.IGNITION_SWITCH_STABLE;
                    bodyInformation.ignitionStatus = IgnitionStatus.RUN;
                    bodyInformation.driverDoorAjar = false;
                    bodyInformation.passengerDoorAjar = false;
                    bodyInformation.rearRightDoorAjar = false;
                    bodyInformation.rearLeftDoorAjar = true;

                    DeviceStatus deviceStatus = new DeviceStatus();
                    deviceStatus.voiceRecOn = true;
                    deviceStatus.btIconOn = true;
                    deviceStatus.callActive = true;
                    deviceStatus.phoneRoaming = false;
                    deviceStatus.textMsgAvailable = true;
                    deviceStatus.battLevelStatus = DeviceLevelStatus.FOUR_LEVEL_BARS;
                    deviceStatus.stereoAudioOutputMuted = false;
                    deviceStatus.monoAudioOutputMuted = false;
                    deviceStatus.signalLevelStatus = DeviceLevelStatus.FOUR_LEVEL_BARS;
                    deviceStatus.primaryAudioSource = PrimaryAudioSource.BLUETOOTH_STEREO_BTST;
                    deviceStatus.eCallEventActive = false;

                    HeadLampStatus headLampStatus = new HeadLampStatus();
                    headLampStatus.lowBeamsOn = true;
                    headLampStatus.highBeamsOn = false;
                    headLampStatus.ambientLightSensorStatus = AmbientLightStatus.DAY;

                    ECallInfo eCallInfo = new ECallInfo();
                    eCallInfo.auxECallNotificationStatus = VehicleDataNotificationStatus.NOT_SUPPORTED;
                    eCallInfo.eCallNotificationStatus = VehicleDataNotificationStatus.NOT_SUPPORTED;
                    eCallInfo.eCallConfirmationStatus = ECallConfirmationStatus.NORMAL;

                    AirbagStatus airbagStatus = new AirbagStatus();
                    airbagStatus.driverAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.driverSideAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.driverCurtainAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.passengerAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.passengerCurtainAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.driverKneeAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.passengerKneeAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
                    airbagStatus.passengerSideAirbagDeployed = VehicleDataEventStatus.NO_EVENT;

                    EmergencyEvent emergencyEvent = new EmergencyEvent();
                    emergencyEvent.emergencyEventType = EmergencyEventType.NO_EVENT;
                    emergencyEvent.fuelCutoffStatus = FuelCutoffStatus.NORMAL_OPERATION;
                    emergencyEvent.rolloverEvent = VehicleDataEventStatus.NO_EVENT;
                    emergencyEvent.maximumChangeVelocity = VehicleDataEventStatus.NO_EVENT;
                    emergencyEvent.multipleEvents = VehicleDataEventStatus.NO_EVENT;

                    ClusterModeStatus clusterModeStatus = new ClusterModeStatus();
                    clusterModeStatus.powerModeActive = true;
                    clusterModeStatus.powerModeQualificationStatus = PowerModeQualificationStatus.POWER_MODE_OK;
                    clusterModeStatus.carModeStatus = CarModeStatus.NORMAL;
                    clusterModeStatus.powerModeStatus = PowerModeStatus.RUNNING_2;

                    MyKey myKey = new MyKey();
                    myKey.e911Override = VehicleDataStatus.OFF;

                    FuelRange fuelRange = new FuelRange();
                    fuelRange.range = 5;
                    fuelRange.type = FuelType.GASOLINE;

                    List<FuelRange> fuelRangeList = new List<FuelRange>();
                    fuelRangeList.Add(fuelRange);

                    sendRpc(BuildRpc.buildVehicleInfoGetVehicleDataResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS,
                                                                           gPSData, 100, 1000, -6, ComponentVolumeStatus.NORMAL, 0, -40, "VIN0123456789FFFF", PRNDL.DRIVE, tireStatus,
                                                                           11431, beltStatus, bodyInformation, deviceStatus, VehicleDataEventStatus.NO_EVENT, WiperStatus.AUTO_ADJUST, headLampStatus, -1000, 25, -2000,
                                                                            eCallInfo, airbagStatus, emergencyEvent, clusterModeStatus, myKey, ElectronicParkBrakeStatus.OPEN, 10, fuelRangeList));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVehicleInfoGetVehicleTypeRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetVehicleType msg)
		{
            if (appSetting.getVIGetVehicleType())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(((MainActivity)appUiCallback), tmpObj.getMethod());
				if (null == tmpObj)
				{
                    VehicleType vehicleType = new VehicleType
                    {
                        make = "Ford",
                        model = "Edge",
                        trim = "SEL",
                        modelYear = "2017"
                    };

                    sendRpc(BuildRpc.buildVehicleInfoGetVehicleTypeResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, vehicleType));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVehicleInfoReadDIDRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.ReadDID msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVehicleInfoReadDIDResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoSubscribeVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.SubscribeVehicleData msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVehicleInfoSubscribeVehicleDataResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, 
                                                                              null, null, null, null, null, null,
                                                                              null, null, null, null, null, null,
                                                                              null, null, null, null, null, null,
                                                                              null, null, null, null, null, null, 
                                                                              null, null, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoUnsubscribeVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.UnsubscribeVehicleData msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildVehicleInfoUnsubscribeVehicleDataResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS,
																			  null, null, null, null, null, null,
																			  null, null, null, null, null, null,
																			  null, null, null, null, null, null,
																			  null, null, null, null, null, null, 
                                                                              null, null, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcPutfileNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnPutFile msg)
		{
			Dictionary<string, Bitmap> tmpMapping = new Dictionary<string, Bitmap>();
			string storedFileName = HttpUtility.getStoredFileName(msg.getSyncFileName());
			int appId = HttpUtility.getAppId(storedFileName);
			string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);
			string fileName = HttpUtility.getFileName(storedFileName);

			Stream inputStream = HttpUtility.downloadFile(storedFileName);

			String state = Android.OS.Environment.ExternalStorageState;

			if (Android.OS.Environment.MediaMounted == state)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";
				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (!sharpHmiAndroidDir.Exists())
				{
					sharpHmiAndroidDir.Mkdir();
				}

				Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
				if (!appStorageDir.Exists())
				{
					appStorageDir.Mkdir();
				}

				Java.IO.File myFile = new Java.IO.File(appStorageDir, fileName);

				if (!myFile.Exists())
				{
					try
					{
						myFile.CreateNewFile();
						FileOutputStream fileOutStream = new FileOutputStream(myFile);

						byte[] buffer = new byte[1024];
						int len = 0;
						while ((len = inputStream.Read(buffer, 0, buffer.Length)) > 0)
						{
							fileOutStream.Write(buffer, 0, len);
						}
						fileOutStream.Close();
					}
					catch (Exception ex)
					{
					}
				}
			}

			if (appIdPutfileList.ContainsKey(appId))
			{
				if (!appIdPutfileList[appId].Contains(msg.getSyncFileName()))
				{
					appIdPutfileList[appId].Add(msg.getSyncFileName());
				}
			}
			else
			{
				List<string> tmpPutFileList = new List<string>();
				tmpPutFileList.Add(msg.getSyncFileName());
				appIdPutfileList.Add(appId, tmpPutFileList);
			}

		}

		public override void onBcAllowDeviceToConnectRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.AllowDeviceToConnect msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, true));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcDialNumberRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.DialNumber msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationDialNumberResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcGetSystemInfoRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.GetSystemInfo msg)
		{
            if (appSetting.getBCGetSystemInfo())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(((MainActivity)appUiCallback), tmpObj.getMethod());
                if (null == tmpObj)
                {
                    sendRpc(BuildRpc.buildBasicCommunicationGetSystemInfoResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, "CCPU VERSION", Language.EN_US, "WERS_CODE"));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
		}

		public override void onBcPolicyUpdateRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.PolicyUpdate msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationPolicyUpdateResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcSystemRequestRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.SystemRequest msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationSystemRequestResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcUpdateAppListRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.UpdateAppList msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationUpdateAppListResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcUpdateDeviceListRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.UpdateDeviceList msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildBasicCommunicationUpdateDeviceListResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public Stream getPutfile(string syncFileName)
		{
			Stream inputStreamObj = null;
			if (Android.OS.Environment.MediaMounted == Android.OS.Environment.ExternalStorageState)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";

				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (sharpHmiAndroidDir.Exists())
				{
					string storedFileName = HttpUtility.getStoredFileName(syncFileName);
					string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);

					Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
					if (appStorageDir.Exists())
					{
						string fileName = HttpUtility.getFileName(storedFileName);

						Java.IO.File myFile = new Java.IO.File(appStorageDir, fileName);
						if (myFile.Exists())
						{
							StreamReader inputStream = new StreamReader(appRootDirPath + appStorageDirectoryName + "/" + fileName);
							inputStreamObj = inputStream.BaseStream;
						}
					}
				}
			}
			return inputStreamObj;
		}

		public void deletePutfileDirectory(string syncFileName)
		{
			if (Android.OS.Environment.MediaMounted == Android.OS.Environment.ExternalStorageState)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";

				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (sharpHmiAndroidDir.Exists())
				{
					string storedFileName = HttpUtility.getStoredFileName(syncFileName);
					string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);

					Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
					if (appStorageDir.Exists() && appStorageDir.IsDirectory)
					{
						Java.IO.File[] filesInFolder = appStorageDir.ListFiles();

						bool filedeleted = false;
						for (int i = 0; i < filesInFolder.Length; i++)
						{
							Java.IO.File file = filesInFolder[i];
							filedeleted = file.Delete();
						}
						filedeleted = appStorageDir.Delete();
					}
				}
			}
		}

        public override void onNavOnAudioDataStreamingNotification(OnAudioDataStreaming msg)
        {

        }

        public override void onNavOnVideoDataStreamingNotification(OnVideoDataStreaming msg)
        {

        }

        public override void onNavOnWayPointChangeNotification(OnWayPointChange msg)
        {

        }

        public override void onBcOnResumeAudioSourceNotification(OnResumeAudioSource msg)
        {

        }

        public override void onBcOnSDLPersistenceCompleteNotification(OnSDLPersistenceComplete msg)
        {

        }

        public override void onBcOnFileRemovedNotification(OnFileRemoved msg)
        {

        }

        public override void onBcOnSDLCloseNotification(OnSDLClose msg)
        {

        }

        public override void onBcDecryptCertificateRequest(DecryptCertificate msg)
        {

        }

        public override void onSDLActivateAppResponse(HmiApiLib.Controllers.SDL.IncomingResponses.ActivateApp msg)
        {

        }

        public override void OnSDLOnAppPermissionChangedNotification(OnAppPermissionChanged msg)
        {

        }

        public override void OnSDLOnSDLConsentNeededNotification(OnSDLConsentNeeded msg)
        {

        }

        public override void OnSDLOnStatusUpdateNotification(OnStatusUpdate msg)
        {

        }

        public override void OnSDLOnSystemErrorNotification(OnSystemError msg)
        {

        }

        public override void OnSDLAddStatisticsInfoNotification(AddStatisticsInfo msg)
        {

        }

        public override void OnSDLOnDeviceStateChangedNotification(OnDeviceStateChanged msg)
        {

        }

        public override void onRcGetCapabilitiesRequest(HmiApiLib.Controllers.RC.IncomingRequests.GetCapabilities msg)
        {
			int corrId = msg.getId();

            HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
                HmiApiLib.Common.Structs.RemoteControlCapabilities remoteControlCapabilities = new HmiApiLib.Common.Structs.RemoteControlCapabilities();
                HmiApiLib.Common.Structs.ClimateControlCapabilities climateControlCapabilities = new HmiApiLib.Common.Structs.ClimateControlCapabilities();

                climateControlCapabilities.moduleName = "Climate";
                climateControlCapabilities.currentTemperatureAvailable = true;
                climateControlCapabilities.fanSpeedAvailable = true;
                climateControlCapabilities.desiredTemperatureAvailable = true;
                climateControlCapabilities.acEnableAvailable = true;
                climateControlCapabilities.acMaxEnableAvailable = true;
                climateControlCapabilities.circulateAirEnableAvailable = true;
                climateControlCapabilities.autoModeEnableAvailable = true;
                climateControlCapabilities.dualModeEnableAvailable = true;
                climateControlCapabilities.defrostZoneAvailable = true;
                climateControlCapabilities.ventilationModeAvailable = true;
                List<DefrostZone> defrostZoneList = new List<DefrostZone>();
                defrostZoneList.Add(DefrostZone.FRONT);
                defrostZoneList.Add(DefrostZone.REAR);
                defrostZoneList.Add(DefrostZone.ALL);
                defrostZoneList.Add(DefrostZone.NONE);
                climateControlCapabilities.defrostZone = defrostZoneList;

                List<VentilationMode> ventilationModeList = new List<VentilationMode>();
                ventilationModeList.Add(VentilationMode.UPPER);
                ventilationModeList.Add(VentilationMode.LOWER);
                ventilationModeList.Add(VentilationMode.BOTH);
                ventilationModeList.Add(VentilationMode.NONE);
                climateControlCapabilities.ventilationMode = ventilationModeList;
                List<ClimateControlCapabilities> climateControlCapabilitiesList = new List<ClimateControlCapabilities>();      
                climateControlCapabilitiesList.Add(climateControlCapabilities);
                remoteControlCapabilities.climateControlCapabilities = climateControlCapabilitiesList;

                HmiApiLib.Common.Structs.ButtonCapabilities caps0 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps1 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps2 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps3 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps4 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps5 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps6 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps7 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps8 = new HmiApiLib.Common.Structs.ButtonCapabilities();
                HmiApiLib.Common.Structs.ButtonCapabilities caps9 = new HmiApiLib.Common.Structs.ButtonCapabilities();

                caps0.name = ButtonName.SEEKLEFT;
                caps0.shortPressAvailable = true;
                caps0.longPressAvailable = true;
                caps0.upDownAvailable = true;

                caps1.name = ButtonName.SEEKRIGHT;
                caps1.shortPressAvailable = true;
                caps1.longPressAvailable = true;
                caps1.upDownAvailable = true;

                caps2.name = ButtonName.OK;
                caps2.shortPressAvailable = true;
                caps2.longPressAvailable = true;
                caps2.upDownAvailable = true;

                caps3.name = ButtonName.TUNEDOWN;
                caps3.shortPressAvailable = true;
                caps3.longPressAvailable = true;
                caps3.upDownAvailable = true;

                caps4.name = ButtonName.TUNEUP;
                caps4.shortPressAvailable = true;
                caps4.longPressAvailable = true;
                caps4.upDownAvailable = true;

                caps5.name = ButtonName.PRESET_0;
                caps5.shortPressAvailable = true;
                caps5.longPressAvailable = true;
                caps5.upDownAvailable = true;

                caps6.name = ButtonName.PRESET_1;
                caps6.shortPressAvailable = true;
                caps6.longPressAvailable = true;
                caps6.upDownAvailable = true;

                caps7.name = ButtonName.CUSTOM_BUTTON;
                caps7.shortPressAvailable = true;
                caps7.longPressAvailable = true;
                caps7.upDownAvailable = true;

                caps8.name = ButtonName.AC_MAX;
                caps8.shortPressAvailable = true;
                caps8.longPressAvailable = true;
                caps8.upDownAvailable = true;

                caps9.name = ButtonName.AC;
                caps9.shortPressAvailable = true;
                caps9.longPressAvailable = true;
                caps9.upDownAvailable = true;

                List<ButtonCapabilities> buttonCapabilitiesList = new List<ButtonCapabilities>
                {
                    caps0,
                    caps1,
                    caps2,
                    caps3,
                    caps4,
                    caps5,
                    caps6,
                    caps7,
                    caps8,
                    caps9
                };

                remoteControlCapabilities.buttonCapabilities = buttonCapabilitiesList;


                sendRpc(BuildRpc.buildRcGetCapabilitiesResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, remoteControlCapabilities));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcGetInteriorVehicleDataRequest(GetInteriorVehicleData msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildRcGetInteriorVehicleDataResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null, false));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcGetInteriorVehicleDataConsentRequest(GetInteriorVehicleDataConsent msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildRcGetInteriorVehicleDataConsentResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, true));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcIsReadyRequest(IsReady msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.IsReady();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.IsReady>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.RC, true, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcSetInteriorVehicleDataRequest(SetInteriorVehicleData msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildRcSetInteriorVehicleDataResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onButtonsButtonPressRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.ButtonPress msg)
        {
			int corrId = msg.getId();

            HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildButtonsButtonPressResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onSDLGetListOfPermissionsResponse(GetListOfPermissions msg)
        {

        }

        public override void onSDLGetStatusUpdateResponse(GetStatusUpdate msg)
        {

        }

        public override void onSDLGetURLSResponse(GetURLS msg)
        {

        }

        public override void onSDLGetUserFriendlyMessageResponse(GetUserFriendlyMessage msg)
        {

        }

        public override void onSDLUpdateSDLResponse(UpdateSDL msg)
        {

        }

        public override void onButtonsGetCapabilitiesRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.GetCapabilities msg, InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities>(((MainActivity)appUiCallback), tmpObj.getMethod());
            if (null == tmpObj)
            {
                List<HmiApiLib.Common.Structs.ButtonCapabilities> capabilities = null;
                HmiApiLib.Common.Structs.PresetBankCapabilities presetBankCapabilities = null;
                HmiApiLib.Common.Enums.Result? result = null;

                if (buttonCapabilitiesResponseParam != null)
                {
                    capabilities = buttonCapabilitiesResponseParam.getCapabilities();
                    presetBankCapabilities = buttonCapabilitiesResponseParam.getPresetBankCapabilities();
                    result = buttonCapabilitiesResponseParam.getResult();
                }
                else
                {
                    capabilities = new List<HmiApiLib.Common.Structs.ButtonCapabilities>();
                    addButtonToList(ButtonName.OK, capabilities);
                    addButtonToList(ButtonName.SEEKLEFT, capabilities);
                    addButtonToList(ButtonName.SEEKRIGHT, capabilities);
                    addButtonToList(ButtonName.TUNEUP, capabilities);
                    addButtonToList(ButtonName.TUNEDOWN, capabilities);
                    addButtonToList(ButtonName.PRESET_0, capabilities);
                    addButtonToList(ButtonName.PRESET_1, capabilities);
                    addButtonToList(ButtonName.PRESET_2, capabilities);
                    addButtonToList(ButtonName.PRESET_3, capabilities);
                    addButtonToList(ButtonName.PRESET_4, capabilities);
                    addButtonToList(ButtonName.PRESET_5, capabilities);
                    addButtonToList(ButtonName.PRESET_6, capabilities);
                    addButtonToList(ButtonName.PRESET_7, capabilities);
                    addButtonToList(ButtonName.PRESET_8, capabilities);
                    addButtonToList(ButtonName.CUSTOM_BUTTON, capabilities);
                    addButtonToList(ButtonName.SEARCH, capabilities);

                    presetBankCapabilities = new HmiApiLib.Common.Structs.PresetBankCapabilities();
                    presetBankCapabilities.onScreenPresetsAvailable = true;

                    result = HmiApiLib.Common.Enums.Result.SUCCESS;

                    RpcResponse caps = BuildRpc.buildButtonsGetCapabilitiesResponse(corrId, capabilities, presetBankCapabilities, result);
                    sendRpc(caps);
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }
    }
}