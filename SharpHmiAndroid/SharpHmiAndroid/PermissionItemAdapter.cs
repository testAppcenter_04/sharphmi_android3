﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    public class PermissionItemAdapter : BaseAdapter<PermissionItem>
    {
        List<PermissionItem> permissionItemList;
        Activity context;

        public PermissionItemAdapter(Activity act, List<PermissionItem> list) : base()
        {
            permissionItemList = list;
            context = act;
        }

        public override PermissionItem this[int position] => permissionItemList[position];

        public override int Count => permissionItemList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? context.LayoutInflater.Inflate(
                Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = permissionItemList[position].getName();

            return view;
        }
    }
}
